# Piwigo API
Piwigo client for [piwigo](http://piwigo.org/) API

## Get started

```
var piwigo = require('piwigo-api');
var client = new piwigo(<PIWIGO_URL>);

// get all tags 
client.getTags(function(err,res){
	console.log(res);
});

//get items for a tag
client.getItemsOfTag(tag,function(err,res){
	console.log(res);
});
````

*note : just get public items and tag.*
