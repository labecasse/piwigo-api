/*
 * A client for piwigo api
 */

const Unirest = require('unirest')
const URL = require('url').URL

class PiwigoApi {
  constructor (base) {
    this.URL = new URL('/ws.php', base).href
    this.cookie = Unirest.jar(true)
  }

  getCategoriesList (catId, next) {
    const query = {
      method: 'pwg.categories.getList',
      format: 'json',
      cat_id: catId
    }
    this._getQuery(query, next)
  }

  getCategoriesImages (catId, next) {
    const query = {
      method: 'pwg.categories.getImages',
      cat_id: catId
    }
    this._getQuery(query, next)
  }

  getImagesInfo (imageId, next) {
    const query = {
      method: 'pwg.images.getInfo',
      image_id: imageId
    }
    this._getQuery(query, next)
  }

  searchImages (query, next) {
    const q = {
      method: 'pwg.images.search',
      query: query
    }

    this._getQuery(q, next)
  }

  sessionLogin (username, password, next) {
    Unirest.post(this.URL)
      .jar(this.cookie)
      .send('username=' + username)
      .send('password=' + password)
      .query({
        method: 'pwg.session.login',
        format: 'json'
      })
      .end(function (res) {
        parseJSON(res, next)
      })
  }

  getTags (next) {
    const query = {
      method: 'pwg.tags.getList'
    }
    this._getQuery(query, next)
  }

  getTagImages (name, next) {
    const query = {
      method: 'pwg.tags.getImages',
      tag_name: name
    }
    this._getQuery(query, next)
  }

  _getQuery (query, next) {
    query.format = 'json'

    Unirest.get(this.URL)
      .type('json')
      .jar(this.cookie)
      .query(query)
      .end(function (res) {
        parseJSON(res, next)
      })
  }
}

function parseJSON (res, next) {
  var JSONBody
  try {
    JSONBody = JSON.parse(res.body)
  } catch (e) {
    const err = new Error(e)
    return next(err)
  }

  return handleStatus(JSONBody, next)
}

function handleStatus (body, next) {
  if (body.stat === 'ok') {
    return next(null, body)
  } else {
    const err = new Error('HTTP CODE ' + body.err + ' ' + body.message)
    return next(err)
  }
}

module.exports = PiwigoApi
